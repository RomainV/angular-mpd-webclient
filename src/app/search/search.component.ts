import { Component, OnInit } from '@angular/core';

import { MpdService } from '../shared/mps-service/mpd.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(
    public mpdService: MpdService,
  ) { }

  query: string;

  ngOnInit() {
  }

  search($event) {
    if ( $event.code === 'Enter') { // FIXME No out of the box enum ?
      this.mpdService.getSearch(this.query);
    }
  }
}
