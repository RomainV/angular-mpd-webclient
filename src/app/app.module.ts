import {BrowserModule, HAMMER_GESTURE_CONFIG, HammerModule} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ControlComponent } from './control/control.component';
import { QueueComponent } from './queue/queue.component';
import { NavigationComponent } from './navigation/navigation.component';
import { HttpClientModule } from '@angular/common/http';
import { SearchComponent } from './search/search.component';
import { BrowseComponent } from './browse/browse.component';
import {DragDropModule} from "@angular/cdk/drag-drop";
import {InjectableRxStompConfig, RxStompService, rxStompServiceFactory} from '@stomp/ng2-stompjs';
import {webSocketConfig} from './config/websocket.config';
import {SharedModule} from './shared/shared.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NavigationTriggerDirective } from './navigation/navigation-trigger.directive';
import {HammerConfig} from './config/hammer.config';
import {ArianneComponent} from './arianne/arianne.component';
import {ArianneDirectoryDirective} from './arianne/arianne-directory.directive';
import { SearchInputComponent } from './search-input/search-input.component';
import { ParametersComponent } from './parameters/parameters.component';
import {MatSliderModule} from '@angular/material/slider';
import { FilterFilePipe } from './browse/filter-file.pipe';

const appRoutes: Routes = [
  { path: '', redirectTo: '/queue', pathMatch: 'full' },
  { path: 'queue', component: QueueComponent },
  { path: 'search', component: SearchComponent },
  { path: 'file', component: BrowseComponent },
  { path: 'parameters', component: ParametersComponent },
];

@NgModule({
    declarations: [
        AppComponent,
        ControlComponent,
        QueueComponent,
        NavigationComponent,
        SearchComponent,
        BrowseComponent,
        NavigationTriggerDirective,
        ArianneComponent,
        ArianneDirectoryDirective,
        SearchInputComponent,
        ParametersComponent,
        FilterFilePipe,
    ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: false}),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    /* App global module */
    SharedModule,
    DragDropModule,
    HammerModule,
    FontAwesomeModule,
    MatSliderModule
  ],
  entryComponents: [
  ],
  providers: [
    {
      provide: InjectableRxStompConfig,
      useValue: webSocketConfig
    },
    {
      provide: RxStompService,
      useFactory: rxStompServiceFactory,
      deps: [InjectableRxStompConfig]
    },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: HammerConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
