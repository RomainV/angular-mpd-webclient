import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-arianne',
  templateUrl: './arianne.component.html',
  styleUrls: ['./arianne.component.css'],
  animations: [
    trigger('select', [
      state('selected', style({
        color: 'blue',
      })),
      state('unselected', style({
        color: 'black',
      })),
      state('hover', style({
        color: 'red',
      })),
      transition('* <=> *', [
        animate('100ms')
      ]),
    ])
  ]
})
export class ArianneComponent implements OnInit {

  @Input()
  set path(path: String) {
    if( path ) {
      if ( path.startsWith('/') ) {
        path = path.substr(1);
      }
      if (path) {
        this.parsedPath = path.split('/');
      } else {
        this.parsedPath = [];
      }
      // Select last dir in path
      this.currentDirIndex = this.parsedPath ? this.parsedPath.length : 0;
    } else {
      this.parsedPath = [];
      this.currentDirIndex = 0;
    }
    this.parsedPath.unshift('root');
  }

  @Output()
  private navigate: EventEmitter<string> = new EventEmitter<string>();

  public parsedPath: string[];
  public currentDirIndex: number;
  public selectedDir: number;

  constructor() { }

  ngOnInit(): void {
  }

  navigateTo(index: number): void {
    if ( index === 0 ) {
      this.navigate.emit('/');
    } else {
      let part: string[] = this.parsedPath.slice(1, index+1);
      this.navigate.emit(part.join('/'));
    }
  }

  @HostListener('panEnd', ['$event'])
  onPanEnd($event) {
    if ( $event.deltaX < (300) ) {
      this.selectedDir = this.currentDirIndex - 1;
    }
    $event.preventDefault();

    if ( this.selectedDir !== this.currentDirIndex) {
      this.navigateTo(this.selectedDir);
    }
    this.selectedDir = null;
  }

}
