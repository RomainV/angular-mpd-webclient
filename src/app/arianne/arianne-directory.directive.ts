import {Directive, ElementRef, Input} from '@angular/core';

@Directive({selector: 'arianne-directory'})
export class ArianneDirectoryDirective {
  constructor(public el: ElementRef) {}
  @Input() id: number;
}
