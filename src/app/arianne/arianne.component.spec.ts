import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArianneComponent } from './arianne.component';

describe('ArianneComponent', () => {
  let component: ArianneComponent;
  let fixture: ComponentFixture<ArianneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArianneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArianneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
