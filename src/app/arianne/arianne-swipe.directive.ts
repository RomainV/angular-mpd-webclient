import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[arianneSwipe]'
})
export class ArianneSwipeDirective {
  private panDelta: number = 0;

  @HostBinding('style.transform')
  styleTransform:string;

  constructor() { }

}
