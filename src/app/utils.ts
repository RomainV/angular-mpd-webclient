
export class Utils {

    static secondsToHms(d: number) {
        let m:number = Math.floor(d / 60);
        let s:number = Math.floor(d % 60);

        return ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
    }

}
