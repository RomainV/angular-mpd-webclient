import { Pipe, PipeTransform } from '@angular/core';
import {MpdDataFile} from '../shared/models/mpdDataBrowse.model';

@Pipe({
  name: 'filterfile'
})
export class FilterFilePipe implements PipeTransform {

  transform(value: MpdDataFile[], arg: string): MpdDataFile[] {
    if ( value != null && arg != null && arg.length > 2) {
      return value.filter((file) => file.directory ?
        file.directory.toLowerCase().includes(arg.toLowerCase()) :
        file.file.toLowerCase().includes(arg.toLowerCase()));
    }
    return value;
  }

}
