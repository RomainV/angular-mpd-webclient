import { Component, OnInit } from '@angular/core';

import { MpdService } from '../shared/mps-service/mpd.service';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {MpdDataFile} from '../shared/models/mpdDataBrowse.model';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.component.html',
  styleUrls: ['./browse.component.css']
})
export class BrowseComponent implements OnInit {

  currentPath: string;
  faAdd = faPlus;
  search: string;

  constructor(public mpdService: MpdService) { }

  ngOnInit() {
    this.mpdService.browse('/');
  }

  browseOrAdd(object) {
    if ( object.directory ) {
      this.browseTo(object.directory);
    } else {
      this.mpdService.addUri(object.file);
    }
  }

  browseTo(path:string) {
    if ( path != undefined ) {
      this.mpdService.browse(path);
      this.currentPath = path;
      this.search = null;
    } else {
      this.mpdService.browse('/');
      this.currentPath = null;
      this.search = null;
    }
  }

  public getFileUri(index: number, item: MpdDataFile): string {
    return item.file;
  }

}
