import {Component, OnInit} from '@angular/core';
import {MpdService} from '../shared/mps-service/mpd.service';

import {faExchangeAlt, faTrash} from '@fortawesome/free-solid-svg-icons';
import {CdkDragDrop} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.css']
})
export class QueueComponent implements OnInit {
  faTrash = faTrash;
  faDrag = faExchangeAlt;
  albumArt;
  lastTrack;

  constructor(public mpdService: MpdService) {
  }

  ngOnInit() {
    this.mpdService.getQueue();
    this.mpdService.mpdDataCurrentTrack.subscribe((currentTrack) => {
      if (currentTrack != null && this.lastTrack !== currentTrack.file) {
        this.mpdService.getAlbumArt(currentTrack.file);
        this.lastTrack = currentTrack.file;
      }
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    this.mpdService.move(event.previousIndex, event.currentIndex);
    this.mpdService.getQueue();
  }

}
