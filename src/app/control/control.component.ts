import { Component, OnInit } from '@angular/core';

import { MpdService } from '../shared/mps-service/mpd.service';
import {faStepForward, faStepBackward, faPlay, faPause} from '@fortawesome/free-solid-svg-icons';
import {of, Subscription} from 'rxjs';
import {delay} from 'rxjs/operators';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.css'],
  animations: [
    trigger('fade', [
      state('show', style({
        display: 'flex',
        justifyContent: 'space-around',
        opacity: '60%',
      })),
      state('hidden', style({
        display: 'visible',
        justifyContent: 'space-around',
        opacity: '20%',
      })),
      transition('show => hidden', [
        animate('0.3s')
      ])
    ])
  ],
})
export class ControlComponent implements OnInit {

  constructor(public mpdService: MpdService) { }

  public controlMode: boolean = false;
  private controlTimeout: Subscription;

  /* FA Icons **/
  public stepBackward = faStepBackward;
  public play = faPlay;
  public pause = faPause;
  public stepForward = faStepForward;

  ngOnInit() {
  }

  toggleControl() {
    this.controlMode = !this.controlMode;
    this.delayHideControl();
  }

  clickNext() {
    this.mpdService.setNext();
  }

  clickPrevious() {
    this.mpdService.setPrev();
  }

  clickPlay($event, state: string) {
    if ( state === "play") {
      this.mpdService.setPause();
    } else if ( state === "stop" ) {
      this.mpdService.playTrack(0);
    } else {
      this.mpdService.setPlay();
    }
    this.delayHideControl();
    $event.stopPropagation();
  }

  private delayHideControl() {
    if ( this.controlTimeout ) {
      this.controlTimeout.unsubscribe();
    }
    this.controlTimeout = of(null).pipe(delay(5000)).subscribe( () => {
      console.log('hide control');
      this.controlMode = false;
      this.controlTimeout.unsubscribe();
      this.controlTimeout = null;
    }) ;
  }
}
