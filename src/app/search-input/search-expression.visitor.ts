import {
  BinarySearchExpression,
  ExpressionOperatorEnum, RootSearchExpression,
  SearchExpression, SingleTerminalExpression, TerminalExpression, TypeExpression,
  UnarySearchExpression
} from '../shared/models/search/searchExpression.model';

export class AddExpressionVisitor {
  // public addExpression(rExp: RootSearchExpression, add: SearchExpression): RootSearchExpression {
  //   this.addExpressionRecursivly(rExp, rExp.expression, add)
  //   return rExp;
  // }
  //
  // private addExpressionRecursivly(rExp: RootSearchExpression, exp: SearchExpression, add: SearchExpression): void {
  //   switch (exp.operation) {
  //     case ExpressionOperatorEnum.unitary:
  //       let uExp = <UnarySearchExpression> exp;
  //       uExp.argument ? this.addExpressionRecursivly(rExp, exp, add) : uExp.argument = add;
  //       return;
  //     case ExpressionOperatorEnum.binary:
  //       let bExp = <BinarySearchExpression> exp;
  //   }
  // }
  //
  // private getLocalAst(tokens: Tocken[]) {
  //   let last;
  //   while ( tokens.length > 0 ) {
  //     let t: Tocken = tokens.shift();
  //     if ( t.operation.type === TokensEnum.TerminalStr) {
  //       let exp = new TerminalExpression();
  //       exp.value = t.value;
  //       return exp;
  //     } if ( t.operation.type === TokensEnum.TypeToken ) {
  //       let exp = new TypeExpression()
  //       exp.value = t.value;
  //       last = exp;
  //     } if (t.operation.type === TokensEnum.UnaryOp) {
  //       let uexp = new UnarySearchExpression();
  //       uexp.argument = this.getLocalAst(tokens);
  //       return uexp;
  //     }
  //     if (t.operation.type === TokensEnum.BinaryOp || t.operation.type === TokensEnum.LogicalBinaryOp) {
  //       let bexp = new BinarySearchExpression();
  //       bexp.operation = ExpressionOperatorEnum.binary;
  //       bexp.type = t.operation.operation;
  //       bexp.firstArgument = last;
  //       bexp.secondArgument = this.getLocalAst(tokens);
  //       return bexp;
  //     }
  //   }
  //
  // private createAst(tokens: Tocken[]): SearchExpression {
  //     let last = new SearchExpression();
  //     while ( tokens.length > 0 ) {
  //       let t:Tocken = tokens.shift();
  //       if ( t.operation.type === TokensEnum.TypeToken ) {
  //         let exp = new TypeExpression()
  //         exp.value = t.operation.operation;
  //         last = exp;
  //       } else if ( t.operation.type === TokensEnum.TerminalStr) {
  //         let exp = new TerminalExpression()
  //         exp.value = t.value.trim();
  //         last = exp;
  //       } else if ( t.operation.type === TokensEnum.UnaryOp ) {
  //         let uexp = new UnarySearchExpression();
  //         uexp.type = t.operation.operation;
  //         uexp.argument = this.getLocalAst(tokens);
  //         last = uexp;
  //       } else if ( t.operation.type === TokensEnum.BinaryOp || t.operation.type === TokensEnum.LogicalBinaryOp ) {
  //         let bexp = new BinarySearchExpression();
  //         bexp.type = t.operation.operation;
  //         bexp.firstArgument = last;
  //         bexp.secondArgument = this.getLocalAst(tokens);
  //         last = bexp;
  //       } else if  (t.operation.type === TokensEnum.SingleString) {
  //         let exp = new SingleTerminalExpression()
  //         exp.value = t.value.trim();
  //         last = exp;
  //       }
  //     }
  //     return last;
  //   }
  // };
}
