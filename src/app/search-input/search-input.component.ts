import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {
  ExpressionTypeEnum,
  SearchExpression,
} from '../shared/models/search/searchExpression.model';
import {MpdService} from '../shared/mps-service/mpd.service';

class Tocken {
  public operation: SearchOperation;
  public value: string;
}

enum TokensEnum {
  TypeToken,
  SingleString,
  BinaryOp,
  LogicalBinaryOp,
  UnaryOp,
  TerminalStr,
}

class SearchOperation {
  public operation: ExpressionTypeEnum;
  public type: TokensEnum;
  public color: string;
}

@Component({
  selector: ' app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.css']
})
export class SearchInputComponent implements OnInit {
  public ExpressionTypeEnum = ExpressionTypeEnum;

  @ViewChild('searchInput') searchInput: ElementRef;

  public query: string;
  public expression: SearchExpression;
  public tokens: Tocken[] = [];

  public possibleChoice: string[];

  public last: string;

  @Output() searchChange = new EventEmitter<SearchExpression>();

  private operationList: SearchOperation[] = [
    {operation: ExpressionTypeEnum.Artiste, type: TokensEnum.TypeToken, color: '#5bab3c' },
    {operation: ExpressionTypeEnum.Album, type: TokensEnum.TypeToken, color: '#51bebc' },
    {operation: ExpressionTypeEnum.SongTitle, type: TokensEnum.TypeToken, color: '#d0aa63' },
    {operation: ExpressionTypeEnum.Equal, type: TokensEnum.BinaryOp, color: '#807f7a' },
    {operation: ExpressionTypeEnum.And, type: TokensEnum.LogicalBinaryOp, color: '#9d9992' },
    // {operation: ExpressionTypeEnum.Or, type: TokensEnum.LogicalBinaryOp, color: '#a4a099' },
    {operation: ExpressionTypeEnum.Not, type: TokensEnum.UnaryOp, color: '#b07680' },
  ];

  showAutocomplete: boolean = false;

  constructor(
    private mpdService:MpdService,
  ) { }

  ngOnInit(): void {
    this.possibleChoice = this.computeFilterList();
  }

  public changeFilter($event): void {
    console.debug($event);
    // Compute filter on double separator & delete on double backspace
    if ( $event.key === " " && this.last === " ") {
      this.addToken();
    } else if ( $event.key === 'Backspace' && this.last === "Backspace" && this.query == '') {
      this.deleteToken();
    } else if ( $event.key === 'Enter' ) {
      this.addToken();
      this.submitTokens();
    }

    console.debug(this.query, this.tokens.length, this.query && this.tokens.length )
    this.possibleChoice = this.computeFilterList();

    this.last = $event.key;
  }

  /**
   * @private Implements following grammar
   *
   *  None        -> TypeToken  | SingleString
   *  TypeTocken  -> BinaryOp
   *  BinaryOp    -> TerminalStr| UnaryOp
   *  UnaryOp     -> UnaryOp    | TerminalStr
   *  TerminalStr -> End        | BinaryOp
   *  SingleString-> End
   *
   */
  private computeFilterList(): string[] {
    let allowedTokens: TokensEnum[] = [];
    if ( this.tokens.length > 0) {
      let lastOperation: SearchOperation = this.tokens[this.tokens.length - 1].operation;
      switch (lastOperation.type) {
        case TokensEnum.TypeToken:
          allowedTokens = [TokensEnum.BinaryOp]
          break;
        case TokensEnum.LogicalBinaryOp:
          allowedTokens = [TokensEnum.TypeToken];
          break;
        case TokensEnum.BinaryOp:
        case TokensEnum.UnaryOp:
          allowedTokens = [TokensEnum.UnaryOp, TokensEnum.TerminalStr];
          break;
        case TokensEnum.TerminalStr:
          allowedTokens = [TokensEnum.LogicalBinaryOp]
          break;
        case TokensEnum.SingleString:
          allowedTokens = []
      }
    } else {
       allowedTokens = [TokensEnum.TypeToken, TokensEnum.SingleString]
    }

    let allowedOperation = this.getAllOperationFromTokenType(allowedTokens);
    return allowedOperation.filter( value => this.query ? value.startsWith(this.query) : true);
  }

  private getAllOperationFromTokenType(allowedTokens: TokensEnum[]): string[] {
    return this.operationList
      .filter( (op) => allowedTokens.includes(op.type))
      .map( (op) => op.operation.readableName);
  }


  private operationMatcher(query: string): SearchOperation | null {
    let operation: SearchOperation = this.operationList.find((op) => op.operation.readableName.toString().toLowerCase() === query.trim().toLowerCase());
    if ( operation == null ) {
      operation = new SearchOperation();
      operation.type = this.tokens.length ? TokensEnum.TerminalStr : TokensEnum.SingleString;
      operation.operation = ExpressionTypeEnum.SimpleText;
    }

    return operation;
  }

  private addToken() {
    if ( this.query ) {
      let tocken: Tocken = new Tocken();
      tocken.operation = this.operationMatcher(this.query);
      tocken.value = this.query;
      this.query = "";
      this.tokens.push(tocken);
    }
  }

  private deleteToken() {
    let lastToken = this.tokens.pop();
    if ( lastToken != null ) {
      this.query = lastToken.value + ' ';
    }
  }

  private submitTokens() {
    let searchExpression: string = this.tokenToString();
    console.debug("Submiting - ", searchExpression);
    this.mpdService.getSearch(searchExpression);
  }

  addSelectedToken($event, token: string) {
    let tocken: Tocken = new Tocken();
    tocken.operation = this.operationMatcher(token);
    tocken.value = token.trim();
    this.query = "";
    this.tokens.push(tocken);
    this.possibleChoice = this.computeFilterList();
    $event.stopPropagation();
    console.debug('adding', tocken)
  }

  private tokenToString(): string {
    let lastExp: string = "(";
    // Deep copy
    console.debug(this.tokens);
    let tokensCopy = JSON.parse(JSON.stringify(this.tokens));

    while ( tokensCopy.length > 0) {
      lastExp = lastExp + this.tokenToStringRec("", tokensCopy);
    }
    return lastExp + ")";
  }

  private tokenToStringRec(last, tokens: Tocken[]): string {
    let token: Tocken = tokens.shift();
    if ( token.operation.type == TokensEnum.SingleString ) {
      console.debug("TokensEnum.SingleString")
      return '(any contains "' + token.value  + '") ';
    } else if ( token.operation.type == TokensEnum.TypeToken ) {
      console.debug("TokensEnum.TypeToken")
      let part = last + "(" + token.operation.operation.name + " ";
      return this.tokenToStringRec(part, tokens);
    } else if ( token.operation.type == TokensEnum.BinaryOp
                || token.operation.type == TokensEnum.LogicalBinaryOp ) {
      console.debug("TokensEnum.BinaryOp")
      let part = last + token.operation.operation.name + " "
      return this.tokenToStringRec(part, tokens);
    } else if ( token.operation.type == TokensEnum.TerminalStr ) {
      console.debug("TokensEnum.TerminalStr")
      return last + '"' + token.value.trim() + '") '
    }
    if ( tokens.length > 0) {
      console.debug("Tokens restant")
      return this.tokenToStringRec(last, tokens);
    }
  }
}
