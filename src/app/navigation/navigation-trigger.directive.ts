import {Directive, HostListener} from '@angular/core';
import {NavigationService} from './navigation.service';

@Directive({
  selector: '[appNavigationTrigger]'
})
export class NavigationTriggerDirective {

  constructor(
    private navigationService: NavigationService
  ) { }

  @HostListener('panMove', ['$event'])
  onPan($event) {
    this.navigationService.panChange($event);
    $event.preventDefault();
  }

  @HostListener('panEnd', ['$event'])
  onPanEnd($event) {
    this.navigationService.panEnd($event);
    $event.preventDefault();
  }
}
