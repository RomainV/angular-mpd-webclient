import { Injectable } from '@angular/core';
import { Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  public event: Subject<any> = new Subject<any>();
  public isMenuOpen: Subject<any> = new Subject<any>();

  constructor() { }

  panChange(event: any) {
    this.event.next(event);
    this.isMenuOpen.next(true);
  }

  panEnd(event: any) {
    this.event.next(event);
    this.isMenuOpen.next(false);
  }
}
