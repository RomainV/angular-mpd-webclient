import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NavigationService} from './navigation.service';
import {Router} from '@angular/router';
import {animate, state, style, transition, trigger} from '@angular/animations';

interface MenuItem {
  name: string;
  link: string;
}

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
  animations: [
    trigger('select', [
      state('selected', style({
        opacity: 1,
        fontSize: '36pt',
        color: 'orange',
      })),
      state('unselected', style({
        opacity: 0.6,
        fontSize: '32pt',
        color: 'white'
      })),
      transition('selected <=> unselected', [
        animate('0.2s')
      ])
    ]),
  ],
})
export class NavigationComponent implements OnInit {
  public menuItems: MenuItem[] = [
    {name:'Rechercher', link:'search'},
    {name:'Écouter', link:'queue'},
    {name:'Fichier', link:'file'},
    {name:'Paramétrer', link:'parameters'},
    {name:'Plus', link:''}
  ];

  public showMenu: boolean;
  public selectedItem: number;

  @ViewChild('menuWheel')
  private menuWheel: ElementRef;

  @ViewChild('menuSelector')
  private menuSelector: ElementRef;

  constructor(
    private navigationService: NavigationService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.navigationService.isMenuOpen.subscribe( (isOpen)  => this.showMenu = isOpen);
    this.navigationService.event.subscribe( (event)  => {
      let oneItemSize = this.menuSelector.nativeElement.offsetHeight / this.menuItems.length;
      // Euclidien division
      this.selectedItem = Math.floor(event.center.y / oneItemSize);
      this.menuWheel.nativeElement.style.transform = 'translateY(' + (-1 * event.center.y/2) + 'px)'
      // Pan event end in menuWheel area ( below there is the cancel zone )
      if ( event.type === "panend" && event.center.y < this.menuSelector.nativeElement.offsetHeight
        && this.menuItems[this.selectedItem]) {
        this.router.navigate([this.menuItems[this.selectedItem].link]);
      }
    });
  }

}
