import { Component, OnInit } from '@angular/core';
import {MpdService} from '../shared/mps-service/mpd.service';
import {MatSliderChange} from '@angular/material/slider';

@Component({
  selector: 'app-parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.css'],
})
export class ParametersComponent implements OnInit {
  public queryUri: string;

  constructor(public mpdService: MpdService) { }

  ngOnInit(): void {
  }

  setVolume($event: MatSliderChange) {
    this.mpdService.setVolume($event.value);
  }

  sendQuery() {
    this.mpdService.addUri(this.queryUri);
  }

  clearQueue() {
    this.mpdService.clear();
  }
}
