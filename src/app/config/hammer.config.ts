import {HammerGestureConfig} from '@angular/platform-browser';
import {Injectable} from '@angular/core';
import * as Hammer from 'hammerjs';

@Injectable({providedIn: 'root'})
export class HammerConfig extends HammerGestureConfig {
  overrides = <any> {
    'pan': { direction: Hammer.DIRECTION_ALL },
  };

  buildHammer(element: HTMLElement) {
    if ( element.hasAttribute("swipable") ) {
      return new Hammer(element, {
        // touchAction: 'auto',
        recognizers: [
          [Hammer.Pan, {direction: Hammer.DIRECTION_HORIZONTAL}]
        ]
      });
    } else {
      return new Hammer(element, {
        // touchAction: 'auto',
        recognizers: [
          [Hammer.Pan, {direction: Hammer.DIRECTION_ALL}]
        ]
      });
    }
  }
}
