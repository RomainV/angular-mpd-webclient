import { Pipe, PipeTransform } from '@angular/core';
import {Utils} from '../../utils';

@Pipe({
  name: 'time'
})
export class Time implements PipeTransform {

  transform(value: number, ...args: any[]): any {
    return Utils.secondsToHms(value);
  }

}
