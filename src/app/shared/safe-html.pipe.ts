import { Pipe } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

// From https://stackoverflow.com/questions/43141534/angular2-base64-sanitizing-unsafe-url-value

@Pipe({name: 'safeHtml'})
export class SafeHtmlPipe {
  constructor(private sanitizer:DomSanitizer){}

  transform(entry) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(entry);
  }
}
