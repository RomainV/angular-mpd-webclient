import {Directive, ElementRef, HostListener, Input} from '@angular/core';
import {animate, AnimationBuilder, AnimationMetadata, AnimationPlayer, style} from '@angular/animations';

@Directive({
  selector: '[swipable]',
})
export class SwipableDirective {

  private static OPEN_SWIPE_DISTANCE: number = 120;
  private static CLOSE_SWIPE_DISTANCE: number = 100;

  @Input('swipable') swipeDistance = '120px';

  private player: AnimationPlayer;
  private open: boolean = false;

  constructor(
    private builder: AnimationBuilder,
    private el: ElementRef
  ) { }

  @HostListener('panEnd', ['$event'])
  onPanEnd($event) {
    if ( $event.deltaX < -(SwipableDirective.OPEN_SWIPE_DISTANCE) ) {
      this.open = true;
    } else if ( $event.deltaX > SwipableDirective.CLOSE_SWIPE_DISTANCE ) {
      this.open = false;
    }
    $event.preventDefault();

    if (this.player) {
      this.player.destroy();
    }

    const metadata = this.open ? this.openRightAnimation() : SwipableDirective.closeRightAnimation();

    const factory = this.builder.build(metadata);
    const player = factory.create(this.el.nativeElement);

    player.play();
  }

  // Définitions des animations de la SwipableDirective

  private openRightAnimation(): AnimationMetadata[] {
    return [
      style({ transform: '*', opacity: '*' }),
      animate('100ms ease-in', style({ transform: 'translateX(-' + this.swipeDistance + ')', opacity: '0.6' })),
    ];
  }

  private static closeRightAnimation(): AnimationMetadata[] {
    return [
      style({ transform: '*', opacity: '*' }),
      animate('100ms ease-in', style({ transform: 'translateX(0px)', opacity: '1' })),
    ];
  }

}
