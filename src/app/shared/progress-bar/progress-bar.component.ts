import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent implements OnInit {
  @Input()
  total: number = 100;

  @Input()
  progress: number = 0;

  @Input()
  control: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
