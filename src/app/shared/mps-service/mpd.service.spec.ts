import { TestBed, inject } from '@angular/core/testing';

import { MpdService } from './mpd.service';

describe('MpdService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MpdService]
    });
  });

  it('should be created', inject([MpdService], (service: MpdService) => {
    expect(service).toBeTruthy();
  }));
});
