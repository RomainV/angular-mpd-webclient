export enum MpdCommand {
  PLAY = 'PLAY',
  DELETE = 'DELETE',
  PAUSE = 'PAUSE',
  STATUS = 'STATUS',
  QUEUE = 'QUEUE',
  SEARCH = 'SEARCH',
  ADD = 'ADD',
  NEXT = 'NEXT',
  PREVIOUS = 'PREVIOUS',
  BROWSE = 'BROWSE',
  VOLUME = 'VOLUME',
  ALBUMART = 'ALBUMART',
  MOVE = 'MOVE',
  CLEAR = 'CLEAR',
}

export interface IMpdMessage {
  command: MpdCommand;
  args: string[];
}
