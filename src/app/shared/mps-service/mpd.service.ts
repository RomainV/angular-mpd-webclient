import {Injectable} from '@angular/core';
import {WebSocketSubject} from 'rxjs/webSocket';

import {MpdDataQueue} from '../models/mpdDataQueue.model';
import {MpdDataState} from '../models/mpdDataState.model';
import {Track} from '../models/track.model';
import {RxStompService} from '@stomp/ng2-stompjs';
import {BehaviorSubject, combineLatest, Observable, Subject, Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {IMpdMessage, MpdCommand} from './mpd-message.interface';
import {MpdDataFile} from '../models/mpdDataBrowse.model';
import {ToasterService} from '../toaster/toaster.service';
import {MpdDataBinary} from '../models/mpdDataBinary.model';

@Injectable({
  providedIn: 'root'
})
export class MpdService {

  public mpdDataQueue: Subject<Track[]> = new BehaviorSubject(null);
  public mpdDataSearch: Subject<Track[]> = new BehaviorSubject(null);
  public mpdDataState: Subject<MpdDataState> = new BehaviorSubject(null);
  public mpdDataBrowse: Subject<MpdDataFile[]> = new BehaviorSubject(null);
  public mpdDataAlbumArt: Subject<MpdDataBinary> = new Subject();
  public mpdLastDataAlbumArt: Subject<string> = new BehaviorSubject(null);
  public mpdDataCurrentTrack: Observable<Track> = new Observable();

  public messages: WebSocketSubject<any>;
  private subscription: Subscription;

  constructor(
    private rxStompService: RxStompService,
    private toasterService: ToasterService,
  ) {
    console.log('Construct a mpdService');

    this.subscription = this.rxStompService.watch('/topic/news').subscribe(
      (data) => this.parseData(JSON.parse(data.body)),
      (err) => console.error(err),
      () => console.log('complete')
    );

    this.initCurrentTrack();
  }

  initCurrentTrack() {

    this.mpdDataCurrentTrack =
      combineLatest([this.mpdDataState, this.mpdDataQueue])
        .pipe(map(([state, queue]) => {
          if (queue != null && state != null) {
            return queue.find(track => track.id === state.songId);
          }
        }));
  }

  private parseData(data) {
    console.log('data receive:', data);
    // let mpdData = undefined;
    let dataObj;
    try {
      dataObj = data.mpdBody;
    } catch (err) {
      console.log(err);
      console.log('data', data);
      dataObj = {responseType: 'error', data: []};
    }

    if (dataObj.responseType === 'QUEUE') {
      const newMpdDataQueue = new MpdDataQueue();
      newMpdDataQueue.deserialize(dataObj);
      this.mpdDataQueue.next(newMpdDataQueue.tracks);
    } else if (dataObj.responseType === 'SEARCH') {
      const newMpdDataSearch = new MpdDataQueue();
      newMpdDataSearch.deserialize(dataObj);
      this.mpdDataSearch.next(newMpdDataSearch.tracks);
    } else if (dataObj.responseType === 'STATUS') {
      const newMpdDataState = new MpdDataState();
      newMpdDataState.deserialize(dataObj);
      this.mpdDataState.next(newMpdDataState);
      // console.log('state', newMpdDataState);
    } else if (dataObj.responseType === 'BROWSE') {
      // let mpdDataBrowse = new MpdDataFile();
      // mpdDataBrowse.deserialize(dataObj);
      // console.log('browse', mpdDataBrowse.directories);
      this.mpdDataBrowse.next(dataObj.data);
    } else if (dataObj.responseType === 'ADD') {
      this.toasterService.notifie('Une piste a été ajoutée.');
    } else if (dataObj.responseType === 'ALBUMART') {
      this.mpdDataAlbumArt.next(dataObj);
    } else {
      console.log('Ignore ', dataObj);
    }
    // console.log('mpddata', mpdData);
  }

  private sendCommande(command: IMpdMessage) {
    this.rxStompService.publish({destination: '/app/news', body: JSON.stringify(command)});
  }

  /** Send function **/
  setPlay() {
    console.log('send ', 'PLAY');
    this.sendCommande({command: MpdCommand.PAUSE, args: ['0']});
  }

  setStop() {
    console.log('send ', 'STOP');
    this.messages.next('MPD_API_SET_STOP');
  }

  setPause() {
    console.log('send ', 'PAUSE');
    this.sendCommande({command: MpdCommand.PAUSE, args: ['1']});
  }

  setNext() {
    console.log('send ', 'NEXT');
    this.sendCommande({command: MpdCommand.NEXT, args: null});
  }

  setPrev() {
    console.log('send ', 'PREV');
    this.sendCommande({command: MpdCommand.PREVIOUS, args: null});
  }

  getQueue() {
    console.log('send ', 'QUEUE');
    this.sendCommande({command: MpdCommand.QUEUE, args: null});
  }

  getSearch(searchText: string) {
    console.log('send ', 'SEARCH', searchText);
    this.sendCommande({command: MpdCommand.SEARCH, args: [searchText]});
  }

  addUri(uri: string) {
    console.log('send', 'ADD');
    this.sendCommande({command: MpdCommand.ADD, args: [uri]});
  }

  browse(path: string) {
    console.log('send', 'BROWSE');
    this.sendCommande({command: MpdCommand.BROWSE, args: [path]});
  }

  deleteFromQueue(track: Track) {
    console.log('delete ', 'track');
    this.sendCommande({command: MpdCommand.DELETE, args: [String(track.pos)]});
    this.getQueue();
  }

  playTrack(trackPos: number) {
    this.sendCommande({command: MpdCommand.PLAY, args: [String(trackPos)]});
  }

  setVolume(volume: number) {
    this.sendCommande({command: MpdCommand.VOLUME, args: [String(volume)]});
  }

  move(previousIndex: number, currentIndex: number) {
    console.log('send', 'MOVE ', String(previousIndex), ' ', String(currentIndex));
    this.sendCommande({command: MpdCommand.MOVE, args: [String(previousIndex), String(currentIndex)]});
  }

  clear() {
    this.sendCommande({command: MpdCommand.CLEAR, args: []});
  }

  async getAlbumArt(uri: string) {
    this.sendCommande({command: MpdCommand.ALBUMART, args: [uri, String(0)]});
    let currentArtIndex = 0;
    let totalImage = '';
    // Attendre la réponse
    const s = this.mpdDataAlbumArt.subscribe((a) => {
      currentArtIndex += a.binaryLength;
      if (a.binary) {
        totalImage += atob(a.binary);
      }

      // Tant que le fichier n'est pas complet, on demande la suite
      if (currentArtIndex < a.size) {
        this.sendCommande({command: MpdCommand.ALBUMART, args: [uri, String(currentArtIndex)]});
      } else {
        s.unsubscribe();
        this.mpdLastDataAlbumArt.next(totalImage ? btoa(totalImage) : null);
      }
    });
  }
}
