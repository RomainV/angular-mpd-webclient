import {Component, Input, OnInit} from '@angular/core';
import {MpdDataFile} from '../models/mpdDataBrowse.model';
import {faCompactDisc, faFile, faFileAudio, faFolder} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent implements OnInit {

  public faSong = faCompactDisc;
  public faPlaylist = faFileAudio;
  public faDirectory = faFolder;

  @Input()
  file: MpdDataFile;

  constructor() { }

  ngOnInit(): void {
  }

}
