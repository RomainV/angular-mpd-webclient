import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TrackComponent} from './track/track.component';
import { Time } from './time/time.pipe';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { SwipableDirective } from './swipable/swipable.directive';
import { FileComponent } from './file/file.component';
import { FilenamePipe } from './filename.pipe';
import {HammerModule} from '@angular/platform-browser';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ArianneSwipeDirective} from '../arianne/arianne-swipe.directive';
import { ToasterComponent } from './toaster/toaster.component';
import {SafeHtmlPipe} from './safe-html.pipe';

@NgModule({
  declarations: [
    TrackComponent,
    ProgressBarComponent,
    Time,
    SwipableDirective,
    ArianneSwipeDirective,
    FileComponent,
    FilenamePipe,
    ToasterComponent,
    SafeHtmlPipe,
  ],
  exports: [
    TrackComponent,
    ProgressBarComponent,
    FileComponent,
    SwipableDirective,
    ArianneSwipeDirective,
    ToasterComponent,
    SafeHtmlPipe,
  ],
  imports: [
    CommonModule,
    HammerModule,
    FontAwesomeModule,
  ]
})
export class SharedModule { }
