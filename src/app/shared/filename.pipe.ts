import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filename'
})
export class FilenamePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    return value.indexOf('/') != -1 ? value.substr(value.lastIndexOf('/')+1) : value;
  }

}
