// src/app/shared/models/mpdDataSongChange.model.ts
import {Deserializable} from "./deserializable.model";
import { Track } from './track.model';


export class MpdDataQueue implements Deserializable {
  type: string;

  public tracks: Track[];

  deserialize(input: any) {
    this.tracks = [];
    this.type = 'queue';
    console.log(input);
    for ( let jsonTrack of input.data ) {
      let track = new Track();
      this.tracks.push(track.deserialize(jsonTrack));
    }
    // console.log('tracks', this.tracks);
    return this;
  }
}
