// src/app/shared/models/mpdDataSongChange.model.ts
import {Deserializable} from "./deserializable.model";
import { SearchResult } from './searchResult.model';


export class MpdDataSearch implements Deserializable {
  type: string;

  public results: SearchResult[];

  deserialize(input: any) {
    this.results = [];
    this.type = 'search';
    console.log(input.data);
    for ( let jsonResult of input.data ) {
      let result = new SearchResult();
      this.results.push(result.deserialize(jsonResult));
    }
    console.log('results', this.results);
    return this;
  }
}
