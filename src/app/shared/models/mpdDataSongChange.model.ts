// src/app/shared/models/mpdDataSongChange.model.ts
import {Deserializable} from "./deserializable.model";

export class MpdDataSongChange implements Deserializable {
  type: string;

  pos: number;
  title: string;
  artist: string;
  album: string;

  deserialize(input: any) {
    this.type = 'song_change';
    Object.assign(this, input.data);
    return this;
  }
}
