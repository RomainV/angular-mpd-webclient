// src/app/shared/models/mpdDataSongChange.model.ts
import {Deserializable} from "./deserializable.model";
import { FileDirectory } from './file.model';
import { FileSong } from './file.model';
import {MpdDataQueue} from './mpdDataQueue.model';
import {Track} from './track.model';


export class MpdDataFile extends Track implements Deserializable {
  type: string;

  directory: string;

  deserialize(input: any) {
    // this.directories = [];
    // this.songs = [];
    this.type = 'browse';
    console.log(input.data);
    for ( let jsonFile of input.data ) {
      if ( jsonFile.type === "directory" ) {
        let file = new FileDirectory();
        // this.directories.push(file.deserialize(jsonFile));
      } else if ( jsonFile.type === "song") {
        let song = new FileSong();
        // this.songs.push(song.deserialize(jsonFile));
      }
    }
    // console.log('files', this.directories);
    return this;
  }
}
