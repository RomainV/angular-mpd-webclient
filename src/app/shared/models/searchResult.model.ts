// src/app/shared/models/track.model.ts
import {Deserializable} from "./deserializable.model";


export class SearchResult implements Deserializable {
  album: string;
  artist: string;
  duration: number;
  uri: string;
  type: string;
  title: string;

  public deserialize(input: any) {
    return Object.assign(this, input);
  }
}
