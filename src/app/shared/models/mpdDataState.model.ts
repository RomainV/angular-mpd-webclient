// src/app/shared/models/mpdDataSongChange.model.ts
import {Deserializable} from "./deserializable.model";

export class MpdDataState implements Deserializable {
  type: string;

  state: string;
  volume: number;
  repeat: number;
  single: number;
  crossfade: number;
  consume: number;
  random: number;
  songpos: number;

  // ok
  elapse: number;
  duration: number;
  songId: number;
  song: string;

  deserialize(input: any) {
    // console.log(input);
    Object.assign(this, input);
    return this;
  }
}
