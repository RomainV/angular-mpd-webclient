// src/app/shared/models/track.model.ts
import {Deserializable} from "./deserializable.model";


export class Track implements Deserializable {
  public file: string;
  public lastModified: string; // TODO Date
  public artist: string;
  public album: string;
  public title: string;
  public track: string;
  public date: any; // TODO Date
  public albumArtist: string;
  public time: number;
  public duration: number;
  public pos: number;
  public id: number;

  public deserialize(input: any) {
    return Object.assign(this, input);
  }
}
