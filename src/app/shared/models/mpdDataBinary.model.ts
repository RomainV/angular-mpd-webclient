// src/app/shared/models/mpdDataQueeu.model.ts
import {Deserializable} from "./deserializable.model";

export class MpdDataBinary implements Deserializable {
  binaryLength: number;
  binary: string;
  size: number;
  type: string;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}
