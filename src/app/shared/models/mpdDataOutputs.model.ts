// src/app/shared/models/mpdDataQueeu.model.ts
import {Deserializable} from "./deserializable.model";

export class MpdDataOutputs implements Deserializable {
  type: string;

  data: number;

  deserialize(input: any) {
    this.type = 'outputs';
    this.data = input.data;
    return this;
  }
}
