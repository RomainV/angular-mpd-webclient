// src/app/shared/models/mpdData.model.ts
import {Deserializable} from "./deserializable.model";

export class MpdData implements Deserializable {
  type: string;
  data: any;

  deserialize(input: any) {
    return Object.assign(this, input);
  }
}
