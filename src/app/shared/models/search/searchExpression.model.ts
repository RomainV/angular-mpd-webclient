export enum ExpressionOperatorEnum {
  terminal,
  unitary,
  binary,
}

export class ExpressionTypeEnum {
  // Logical operator
  static Equal = { name:'contains', readableName: 'contient'};
  // Or = { name:'or', readableName: 'ou'};
  static And = { name:'AND', readableName: 'et'};
  static Not = { name:'!', readableName: 'non'};
  // Search filter
  static Artiste = { name:'artist', readableName: 'artiste'};
  static SongTitle = { name:'title', readableName: 'titre'};
  static Album = { name:'album', readableName: 'album'};
  // Simple text
  static SimpleText = { name:'text', readableName: 'text'};

  public name;
  public readableName;
}

export class SearchVisitor {
  s: string  = "";
  visit(expression: SearchExpression) {
    if ( expression instanceof UnarySearchExpression) {
      console.debug("visit exp u");
      let uExp: UnarySearchExpression = <UnarySearchExpression> expression;
      this.s += uExp.type + " ";
    } if ( expression instanceof BinarySearchExpression) {
      console.debug("visit exp b");
      let bExp: BinarySearchExpression = <BinarySearchExpression> expression;
      this.s += bExp.type + " ";
    } if ( expression instanceof TerminalExpression) {
      console.debug("visit exp t");
      let tExp:  TerminalExpression = <TerminalExpression> expression;
      this.s += ' "' + tExp.value.trim() + '" ';
    } if ( expression instanceof TypeExpression) {
      console.debug("visit exp t");
      let tExp: TypeExpression = <TypeExpression> expression;
      this.s += '' + tExp.value + ' ';
    } if ( expression instanceof SingleTerminalExpression) {
      console.debug("visit exp s");
      let sExp: SingleTerminalExpression = <SingleTerminalExpression> expression;
      this.s += '( any contains "' + sExp.value + '") ';
    }
  }
}

export class SearchExpression {
  public operation: ExpressionOperatorEnum;
  public type: ExpressionTypeEnum;

  public accept(visitor: SearchVisitor) {
    visitor.visit(this);
  };
}

export class RootSearchExpression extends SearchExpression{
  public expression: SearchExpression;
}

export class UnarySearchExpression extends SearchExpression {
  public argument: SearchExpression;

  public accept(visitor: SearchVisitor) {
    console.debug("visit unary");
    visitor.visit(this);
    this.argument.accept(visitor);
  };
}

export class BinarySearchExpression extends SearchExpression {
  public firstArgument: SearchExpression;
  public secondArgument: SearchExpression;

  public accept(visitor: SearchVisitor) {
    console.debug("visit binaryExp");
    this.firstArgument.accept(visitor);
    visitor.visit(this);
    this.secondArgument.accept(visitor);
  };
}

export class TerminalExpression extends SearchExpression {
  public value: string;

  public accept(visitor: SearchVisitor) {
    console.debug("visit terminal");
    visitor.visit(this);
  };
}

export class SingleTerminalExpression extends SearchExpression {
  public value: string;

  public accept(visitor: SearchVisitor) {
    console.debug("visit single string");
    visitor.visit(this);
  };
}

export class TypeExpression extends SearchExpression {
  public value;

  public accept(visitor: SearchVisitor) {
    console.debug("visit type");
    visitor.visit(this);
  };
}
