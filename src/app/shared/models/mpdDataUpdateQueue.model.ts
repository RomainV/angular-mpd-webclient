// src/app/shared/models/mpdDataSongChange.model.ts
import {Deserializable} from "./deserializable.model";

export class MpdDataState implements Deserializable {
  type: string;

  data: number;

  deserialize(input: any) {
    this.type = 'outputs';
    this.data = input.data;
    return this;
  }
}
