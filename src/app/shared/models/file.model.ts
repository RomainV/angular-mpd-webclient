// src/app/shared/models/file.model.ts
import {Deserializable} from "./deserializable.model";


export class FileDirectory implements Deserializable {
  type: string;
  dir: string;

  public deserialize(input: any) {
    return Object.assign(this, input);
  }
}

export class FileSong implements Deserializable {
  type: string;
  uri: string;
  title: string;

  public deserialize(input: any) {
    return Object.assign(this, input);
  }
}
