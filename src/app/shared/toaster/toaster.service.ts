import { Injectable } from '@angular/core';
import {of, Subject, Subscription} from 'rxjs';
import { Toast } from '../models/toast.model';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {
  public notification: Subject<Toast> = new Subject<Toast>();

  constructor() { }

  public notifie(notification: string) {
    this.notification.next(new Toast(notification));
    let sub: Subscription = of(null).pipe(delay(3000)).subscribe( () => {
      this.notification.next(null);
      sub.unsubscribe();
    }) ;
  }
}
