import {Component, Input, OnInit} from '@angular/core';
import {Track} from '../models/track.model';

@Component({
  selector: 'mpd-track',
  templateUrl: './track.component.html',
  styleUrls: ['./track.component.css']
})
export class TrackComponent implements OnInit {

  @Input()
  public track: Track;

  @Input()
  public emphasize: boolean = false;

  constructor() { }

  ngOnInit() {
  }

}
