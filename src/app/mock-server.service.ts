import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

import {environment} from '../environments/environment';
import {filter, first, switchMap} from 'rxjs/operators';
import {Stomp} from '@stomp/stompjs';



export enum SocketClientState {
  ATTEMPTING, CONNECTED
}

@Injectable({
  providedIn: 'root'
})
export class SocketClientService {
}
